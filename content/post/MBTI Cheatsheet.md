---
title: "MBTI Cheatsheet"
---

A quick word on the Myers-Briggs Type Indicator regarding its validity...

It's worth noting that the MBTI has been subject to criticism and controversy regarding its validity and reliability. Nevertheless, it remains popular in various contexts, including in the workplace and in personal development. The MBTI is not a science. While it does have parallels with the emperically derived Big Five, MBTI is a model and a model only. Like a diagram of an atom model in a textbook isn't an actual atom, MBTI isn't an attempt to distill the _entire_  personality down into a comprehensive codified system. Instead, like the atom model, it is a tool we can use to speak about a few aspects of an abstract subject: the personality.

## What is MBTI?

The (M)yers-(B)riggs (T)ype (I)ndicator is a personality system consisting of 16 personality types. It was developed by Katharine Briggs and her daughter Isabel Myers. It is based on Carl Jung's Psychological Types.

As a model, MBTI describes two aspects of personality: how people perceive the world and make decisions. Though behavior can be derived from these two aspects, the reverse is certainly not true.

## What are the types?

| MBTI | Temperament | Intelligence | Keirsey Type | % of population |
| ---- | ----------- | ------------ | ------------ | --------------- |
| ESTJ | SJ | Logistics | Supervisor | 8 - 12 |
| ISTJ | SJ | Logistics | Inspector | 11 - 14 |
| ESFJ | SJ | Logistics | Provider | 8 - 13 |
| ISFJ | SJ | Logistics | Protector | 9 - 14 |
| ESTP | SP | Tactics | Promoter | 4 - 5 |
| ISTP | SP | Tactics | Crafter | 4 - 6 |
| ESFP | SP | Tactics | Performer | 4 - 9 |
| ISFP | SP | Tactics | Composer | 5 - 9 |
| ENTJ | NT | Strategy | Fieldmarshal | 2 - 5 |
| INTJ | NT | Strategy | Mastermind | 2 - 4 |
| ENTP | NT | Strategy | Inventor | 2 - 5 |
| INTP | NT | Strategy | Architect | 3 - 5 |
| ENFJ | NF | Diplomacy | Teacher | 2 - 5 |
| INFJ | NF | Diplomacy | Counselor | 1 - 3 |
| ENFP | NF | Diplomacy | Champion | 6 - 8 |
| INFP | NF | Diplomacy | Healer | 4 - 5 |

## How does it work?

Each type consists of four dichotomies. A dichotomy ("dividing in two" in Greek) is a whole made of of two opposing parts, like ying and yang.

Each type can also be sorted into another grouping called a temperament. These temperaments have ancient roots originally planted by Plato: Artisan (SP), Guardian (SJ), Idealist (NF), and Rational (NT).

### Dichotomies

#### (E)xtraversion and (I)ntroversion

AKA attitudes

Extraversion and introversion are commonly understood as: an extravert is sociable and an introvert isn't. Within the context of MBTI there is a little more nuance. An extravert draws their energy from acting, then reflecting. An introvert contrasts this by reflecting first, then acting. Though this can extend into social situations, extraversion and introversion describe the relationship between energy and where it's drawn from: the external or internal world.

An extravert will seek breadth whereas an introvert will seek depth.

An extravert will prefer frequent interaction whereas an introvert will prefer substantial interaction.

Extraversion is directed outwards and introversion inwards.

#### (S)ensing and I(N)tuition  

AKA perceiving functions

Sensing and intuition describe how people gather and perceive information around them.

A sensor prefers facts, data, and the concrete. Only information gathered with the five senses is to be trusted.

An intuitive prefers hunches, theory, patterns, and the possible. The five senses are validation, not the source of the information itself.

#### (T)hinking and (F)eeling  

AKA judging functions

Being judging functions, thinking and feeling have to do with how decisions are made. Decisions made by each are equally rational.

Thinkers make decisions with a more detached, logical,  and consistent approach.

Feelers make decisions by putting themselves in the situation, focusing on harmony, and considering the concerns of others.

#### (J)udging and (P)erceiving

AKA lifestyle preferences

The addition to Jung's Psychological Types that differentiates MBTI, lifestyle preferences describe a preference for whether one extraverts their judging or perceiving functions to the world.

Those who prefer the judging functions (thinking or feeling) strive for finality in making decisions. Thinking or feeling are the functions they extravert. Intuition or sensing are directed towards their inner world.

Those who prefer the perceiving functions strive to keep decisions open. Intuition or sensing are the functions they extravert. Thinking or feeling are directed towards their inner world.

### Temperaments

Plato's Republic's four characters, Galen's four humors, Baum's four protagonists in the Wizard of Oz, the four nations of Avatar, Aristotle's four sources of happiness, and the Keirsian temperaments, the idea of four types reaches back into antiquity.

#### What are the temperaments?

| Acronym | | | Words | Tools |
| ------- | - | - | ----- | ----- |
| SP | (S)ensing | (P)erceiving | Concrete | Utilitarian |
| SJ | (S)ensing | (J)uding | Concrete | Cooperator |
| NT | I(N)tuitive | (T)hinking | Abstract | Utilitarian |
| NF | I(N)tuitive | (F)eeling | Abstract | Cooperator |

Much can be said about the temperaments and for that David Keirsey's books should be referenced. Being this is a cheat sheet, this will be a simple breakdown of how words and tools are used by the different temperaments.

#### Abstract vs Concrete

"Abstract" and "Concrete" reference the communication styles used by the Intuitive and Sensing dichotomies, respectively.

Abstract words are...

* Analogic
* Fictional
* Schematic
* Theoretical
* General
* Categorical
* Symbolic
* Figurative

Concrete words are...

* Indicative
* Factual
* Detailed
* Empirical
* Specific
* Elemental
* Signal
* Literal

#### Cooperative vs Utilitarian

Cooperative tool users prioritize getting along with others to get what they want. A cooperative tool user will use tools approved by their social group.

Utilitarian tool users prioritize cost and effort to get what they want. A utilitarian tool user will pick the best tool for the job, regardless of their social group.

Neither tool users only uses their primary tool. Indeed, utilitarian tool use is still considered by more cooperative individuals however it is secondary. The reverse is true of utilitarian tool users.

## Final Words

Overall, the MBTI is a useful tool for understanding certain aspects of personality, although it is not without its limitations. It can help people to understand themselves and others better, and to appreciate the different ways in which people perceive the world and make decisions.
