---
title: "Counter-Balancing Automation and Process: The Human Touch"
categories: ["software engineering"]
tags: ["automation"]
---

Automation, process, guidelines, code styles, and linting all help us do work more efficiently by cutting out monotonous, time-consuming tasks. They are the hallmarks of a well-oiled engineering organization. The question isn't whether or not to lean into these principles but how much to lean into them. Like automotive design or art, computer intelligence can only bring us so far until a human intercedes for the finishing touch.

## Why?

Why not take automation and process to the logical extreme? An automobile designed by a computer in a wind tunnel is aerodynamically superior. Art produced by AI has all the markers of great art. Why don't we see these cars? Why aren't there galleries full of AI produced art? The answer is simply a matter of who consumes and interacts with these end products. Cars designed strictly in a wind tunnel are ugly. AI-produced art lacks drama and more importantly: the human touch.

Software engineering isn't an equation or a simple process you can write down on a blog or in a readme. It's a craft and like any craft is a blend of knowledge, experience, creativity, and logical, pragmatic thinking. Indeed there are times where automation and process can and should be setup and defined, however, whenever a decision is made in favor of these, it must be considered how it will be used by consumers: engineers. This idea is fundamental to programming; computer languages are designed not for the computer, but for the individual using it.

This may mean a PR template that differs from repository to repository. Some projects could be fully automated while others require a much more  manual process. Some code might require very strict testing while other code goes completely untested. This is okay and is to be expected.

**But what about consistency?** It's a good question. Consistency is important when considering the human end of the software equation. Consistency leads to predictability, predictability leads to fewer assumptions, and fewer assumptions lead to fewer bugs. So shouldn't we strive to be as consistent as possible? Yes and no! Like automation and process, close cousins of consistency, we should balance our drive for consistency with pragmatism.

> A foolish consistency is the hobgoblin of little minds, adored by little statesmen and philosophers and divines. With consistency a great soul has simply nothing to do. He may as well concern himself with his shadow on the wall. Speak what you think now in hard words, and to-morrow speak what to-morrow thinks in hard words again, though it contradict every thing you said to-day. — 'Ah, so you shall be sure to be misunderstood.' — Is it so bad, then, to be misunderstood? Pythagoras was misunderstood, and Socrates, and Jesus, and Luther, and Copernicus, and Galileo, and Newton, and every pure and wise spirit that ever took flesh. To be great is to be misunderstood.

Ralph Waldo Emerson

The take-away here is that while automation and strict process are important to efficiency, they shouldn't be implemented blindly. We are not computers and thus we should take advantage of our ability to manage ambiguity where computers can not. The human touch is the glue that binds together automation and process to create a system where all three work in tandem, taking advantage of the strengths of each. With this mindset, we should carefully consider how we balance our consistency.

But how do we go about this?

## How?

Let's use the PR process as an example since it includes elements of automation, process, and the counter-balancing human touch.

### The PR Template

PR templates serve two users: reviewers and contributors.

For reviewers, a good PR template answers questions they may ask if otherwise not documented. For contributors the template serves as a gentle reminder of items that can't realistically be caught by automation. While automation might catch a failing test, it's harder to have automation catch when, for instance, a test wasn't written but should have been. Sure, we can set up coverage reporters but adding those as CircleCI checks tend to create maintenance issues and put too much of an emphasis on coverage; at this point automation starts to work against us.

PR template length should be considered. A template that's too short might not be effective enough. A template that's too long is arguably worse, since both reviewers and contributors tend to skip parts of it; long templates are too noisy.

When maintaining a template, if you find yourself adding to it or if you find it's too long, consider where CircleCI checks can help.

### CircleCI Checks

Good CircleCI checks catch issues you can't enforce in local development or shouldn't enforce in a PR template. Though it's best practice to run tests before opening a PR, that isn't something we can or should enforce. Each craftsperson has a slightly different tool set that suits their individualism. For that reason, we run our tests in CI via CircleCI against a system that closely resembles production. If one of these tests fails, the PR _should_ be blocked and we should investigate the cause.

A bad CircleCI check is one you have to maintain that blocks the PR from being merged, like a coverage reporter. Though these reporters can be very informative and there are cases to use them, a coverage report threshold tends to be a metric that itself needs to be maintained. If you remove code, for example, your coverage will actually drop despite removing code. This is because the overall percentage of code the coverage is running against is decreasing. This decrease will then throw off the math used to calculate the coverage, resulting in a metric that drops below the threshold that needed to be maintained for the check to be performed correctly. For the individual opening the PR this means going back and either adding more coverage, likely to code they didn't touch, or updating the coverage threshold to one below what the team agreed to maintain. In this case, automation is working against process and the human element, all to maintain what is ultimately an arbitrary metric.

Before continuing, it's worth pointing out that the coverage reporter example above is an example to illustrate a point. In an ideal world, we'd have 100% test code coverage. With that said, we should strive for test _quality_ over _quantity_.

It is possible to achieve 100% code coverage without ever identifying a single bug. The closer a code-base gets to 100% test code coverage, the more likely engineers are to take shortcuts to hit that metric. With those shortcuts comes a drop in quality that gives the team a false sense of security. There is a point of diminishing returns when it comes to code coverage. The takeaway and central theme is that we should _balance_ idealism with pragmatism, quantity with quality, and maintenance work with feature work. We care about outcomes over output.

Another consideration for CircleCI checks is simply how long they take to run. We do want to keep our PR cycle time down so we should carefully consider what we run as a blocking check and any blocking checks that fail should fail quickly.

### The Human Touch

The human touch leans into one of our greatest strengths: the ability to handle ambiguity and contextual change. These are both concepts computers struggle with, even in the age of artificial intelligence. Like an automotive designer, engineers can only use previous experience and what has been done before as guidelines for what we should do now and what makes sense going forward. An artist doesn't simply read instructions on how to draw from emotion. These extrapolations can't easily be defined in writing or a set of rules. Instead, a reviewing engineer with knowledge, experience, and context must come in, interact with the code in question, ask questions that can't be summed up in a PR template, and work with the contributing engineer. Each situation is a new situation and with it comes new questions. We must use our ability for extrapolation to catch issues beyond the raw code we're reviewing.

PR templates serve the human touch by answering questions that are going to be asked anyway and can start the process of thinking "what issues could this PR cause". Checks serve the human touch by running monotonous tasks and informing us when there might be a larger issue. The human touch ties this altogether by filling in the gaps that PR templates and checks cannot make up for.

## Conclusion

You can think of automation, process, and the human touch as layers work needs to pass through before release. Each layer filters out different issues and each layer has a set of strengths and weaknesses that must be counter-balanced.

Automation is your first layer and it catches obvious, low-hanging, reproducible, and otherwise monotonous issues; tests, lint checks, ping checks, and monitoring all fit in here. Automation counter-balances our human (in)ability to extrapolate _at scale_ and our tendency to forget minutia.

Process counter-balances automation and the human touch by putting in easy to remember rules and best practice, lists and checks, and serving as a sort of "automation of the team".

The human touch filters out everything else. Should `useEffect` have an initial state? Is this the best library to use? We've added another request. Will we hit the throttling limit? Why doesn't this component have a story and test?

Automation, process, and the human touch balanced in tandem results in an engineering organization that iterates faster, is more educated and experienced, and is comprised of individuals who are capable of making autonomous, pragmatic, and multiplying decisions on their own. Ultimately, the human touch is an acknowledgement of the complexity that surrounds software engineering and an empowerment of the individual to make their own decisions instead of relying purely on automation and process set down by other individuals in a different context.
